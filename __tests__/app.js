/*
 * @Descripttion:
 * @version:
 * @Author: chunwen
 * @Date: 2022-04-15 17:15:26
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2022-04-15 18:47:32
 */
"use strict";
const path = require("path");
// Const assert = require("yeoman-assert");
const helpers = require("yeoman-test");

describe("generator-vue-3-ltc:app", () => {
  beforeAll(() => {
    return helpers
      .run(path.join(__dirname, "../generators/app"))
      .withPrompts({ someAnswer: true });
  });

  // It("creates files", () => {
  //   assert.file(["dummyfile.txt"]);
  // });
});
