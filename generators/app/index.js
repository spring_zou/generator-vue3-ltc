/*
 * @Descripttion:
 * @version:
 * @Author: chunwen
 * @Date: 2022-04-15 17:15:26
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2022-04-16 15:57:00
 */
/* eslint-disable */
"use strict";
const path = require("path");
const mkdirp = require("mkdirp");
const Generator = require("yeoman-generator");
const chalk = require("chalk");
const yosay = require("yosay");
const _ = require("lodash");
_.extend(Generator.prototype, require("yeoman-generator/lib/actions/install"));
module.exports = class extends Generator {
  prompting() {
    // Have Yeoman greet the user.
    this.log(
      yosay(
        `Welcome to the sweet ${chalk.red("generator-vue-3-ltc")} generator!`
      )
    );

    const prompts = [
      // {
      //   type: "confirm",
      //   name: "someAnswer",
      //   message: "Would you like to enable this option?",
      //   default: true
      // },
      {
        type: "input",
        name: "namespace",
        message: "Please input your project namespace, such as @tencent:",
        default: ""
      },
      {
        type: "input",
        name: "name",
        message: "Please input project name:",
        default: "vue3"
      },
      {
        type: "input",
        name: "description",
        message: "Please input project description:",
        default: "vue3 project template"
      },
      {
        type: "input",
        name: "author",
        message: "Author's Name",
        default: ""
      },
      {
        type: "input",
        name: "email",
        message: "Author's Email",
        default: ""
      },
      {
        type: "input",
        name: "license",
        message: "License",
        default: "MIT"
      }
    ];

    return this.prompt(prompts).then(props => {
      // To access props later use this.props.someAnswer;
      this.props = props;
      if (this.props.namespace) {
        this.props.fullName = `${this.props.namespace}/${this.props.name}`;
      } else {
        this.props.fullName = this.props.name;
      }
    });
  }

  // 未匹配任何生命周期方法的非私有方法均在此环节*自动*执行
  default() {
    if (path.basename(this.destinationPath()) !== this.props.name) {
      this.log(`\nYour generator must be inside a folder named
        ${this.props.name}\n
        I will automatically create this folder.\n`);

      mkdirp(this.props.name);
      this.destinationRoot(this.destinationPath(this.props.name));
    }
  }

  writing() {
    this.__writingCopy([
      "mock",
      "public",
      "src",
      ".env.development",
      ".env.production",
      ".env.staging",
      ".gitignore",
      "auto-imports.d.ts",
      "components.d.ts",
      "index.html",
      "LICENSE",
      "mockProdServer.ts",
      "package-lock.json",
      "package.json",
      "README.md",
      "README.zh-CN.md",
      "tsconfig.json",
      "vite.config.ts"
    ]);
    this.__writingCopy("package.json", {
      name: this.props.name,
      fullName: this.props.fullName,
      description: this.props.description,
      author: this.props.author,
      email: this.props.email,
      license: this.props.license
    });
  }
  __writingCopy(filePath, params) {
    const flag = Array.isArray(filePath);
    if (flag) {
      filePath.forEach(item => {
        this.fs.copy(this.templatePath(item), this.destinationPath(item));
      });
    } else {
      this.fs.copyTpl(
        this.templatePath(filePath),
        this.destinationPath(filePath),
        params
      );
    }

  }

  install() {
    this.log("install...");
    this.npmInstall();
  }
};
